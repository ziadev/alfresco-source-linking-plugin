Maurizio Pillitu reported an issue with the Alfresco source jars here: https://issues.alfresco.com/jira/browse/MNT-6496

The source is in a java sub-folder rather than starting at the root of the jars. This causes problems for IntelliJ's automatic source importer.

Looks like it will be resolved for 4.1.4 and 4.2.d. 

If you have automatically associated source jars that don't include the !/java suffix, this plugin will add an additional reference with the appropriate suffix for jars matching alfresco-.\*-source.jar in any of the modules in your current project.

Simply grab the alfresco-source-linking-plugin.jar file from the repo and use the IntelliJ option to install a plugin from disk to reference the jar. 
It will add an "Update Referenced Alfresco Source Jars" menu item to the Tools menu. 
Run this with a project open that has source jars pulled via the maven repo and magically your source will become available during development and debugging in IntelliJ.

Of course this is offered without any waranty. If you do choose to use this plugin or code, you do so at your own risk.
