/*
   Copyright 2013 Zia Consulting, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.ziaconsulting.alfrescosourcelinkingplugin;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.LibraryOrderEntry;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.roots.OrderEntry;
import com.intellij.openapi.roots.OrderRootType;
import com.intellij.openapi.roots.libraries.Library;

import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: binduwavell
 * Date: 4/9/13
 * Time: 1:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class AlfrescoSourceLinkingAction extends AnAction {
    private final static Pattern alfrescoJar = Pattern.compile("^.*/alfresco-[^/]+-sources.jar!/$");
    private final static Pattern alfrescoJavaJar = Pattern.compile("^.*/alfresco-[^/]+-sources.jar!/java$");
    private final static Logger log = Logger.getInstance(AlfrescoSourceLinkingAction.class);

    public void actionPerformed(AnActionEvent e) {
        Project project = e.getProject();
        if (null == project) return;
        ModuleManager manager = ModuleManager.getInstance(project);
        Module[] modules = manager.getModules();
        for (Module module : modules) {
            log.info("Checking Module: " + module.getName());
            ModuleRootManager rootManager = ModuleRootManager.getInstance(module);
            OrderEntry[] entries = rootManager.getOrderEntries();
            for (OrderEntry entry : entries) {
                if (entry instanceof LibraryOrderEntry) {
                    final LibraryOrderEntry libraryEntry = (LibraryOrderEntry)entry;
                    final Library library = libraryEntry.getLibrary();
//                    log.debug("\tEntry: " + libraryEntry.getPresentableName());
                    String[] sourceUrls = entry.getUrls(OrderRootType.SOURCES);
                    String alfrescoJarUrl = null;
                    String alfrescoJavaJarUrl = null;
                    for (String url : sourceUrls) {
//                        log.debug("\t\tURL: " + url);
                        if (alfrescoJar.matcher(url).matches())
                            alfrescoJarUrl = url;
                        if (alfrescoJavaJar.matcher(url).matches())
                            alfrescoJavaJarUrl = url;
                    }
                    if (null != alfrescoJarUrl) {
                        final String finalAlfrescoJarUrl = alfrescoJarUrl;
                        final String finalAlfrescoJavaJarUrl = alfrescoJavaJarUrl;
                        ApplicationManager.getApplication().runWriteAction(new Runnable() {
                            @Override
                            public void run() {
                                Library.ModifiableModel modifiableModel = null;
                                if (library != null) {
                                    log.info("Updating sources for: " + libraryEntry.getPresentableName());
                                    modifiableModel = library.getModifiableModel();
                                    // TODO: Need to be smarter about this, may need to handle multiple
                                    if (null != finalAlfrescoJavaJarUrl)
                                        modifiableModel.removeRoot(finalAlfrescoJavaJarUrl, OrderRootType.SOURCES);
                                    modifiableModel.addRoot(finalAlfrescoJarUrl + "java", OrderRootType.SOURCES);
                                    modifiableModel.commit();
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    @Override
    public void update(AnActionEvent e) {
        e.getPresentation().setEnabled(true);
        //super.update(e);
    }


}
